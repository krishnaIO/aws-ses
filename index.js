// const env = require('dotenv');
const bodyParser = require('body-parser');
const request = require('request');
const express = require("express");
const app = express();
const PORT = 3000;
const sesFunc = require('./ses');

// app.use(bodyParser.urlencoded({limit:'50MB',extended:true}));

app.get('/', (req, res) => {
    res.send(`Welcome to demo of aws ses api`)
})



app.get('/getSign', sesFunc.getSignedHeaders);

app.get('/getIds', sesFunc.getIdentites);


app.get('/sendSeSmail', sesFunc.sendEmail)

app.get('/getIdSDK', sesFunc.getIdentitySDK)

app.get('/sendEmailSDK', sesFunc.sendEmailSDKv2)

app.post('/sendSDK', sesFunc.sendSDK)



// app.get('/sendSESEmail')


app.listen(PORT, () => {
    console.log(`server started on port ${PORT}`);
})

