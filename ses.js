const aws4 = require('aws4');
const request = require("request");
const AWS = require('aws-sdk');
const credentials = new AWS.Credentials("$accces_key", "secret_access_key");
const region = "us-east-1"

// AWS.config.update({
//     accessKeyId: "$accces_key",
//     secretAccessKey: "secret_access_key",
//     region: region
// });

module.exports = {

    getSignedHeaders: (req, res) => {
        const opts = { service: 'ses', path: '/?Action=ListIdentities&Version=2010-12-01' };

        aws4.sign(opts, { accessKeyId: '$accces_key', secretAccessKey: 'secret_access_key' });

        console.log("Signed opts-->", opts);

        //hit this api with request library 
        return opts;
    },
    getIdentites: (req, res) => {

        // aws region : 'us-east-1' is default

        // const opts = { service: 'ses', region: 'us-east-1', path: '/v2/email/account' };

        const opts = { service: 'ses', region: 'us-east-1', path: '/v2/email/identities' };

        //by using service and region aws can  infer the host

        aws4.sign(opts, { accessKeyId: '$accces_key', secretAccessKey: 'secret_access_key' });

        console.log("Signed opts-->", opts);

        console.log("\n\n\n")

        /* 
        collect as params
            Region --> we can make url with this ex: 'https://email.{region}.amazonaws.com?Action={path}'
            access_id---
                        ---->  required for signing the header
            secret_key--

            encode the url
        */

        let url = `https://${opts["headers"]["Host"]}${opts.path}`

        //hit this api with request library 
        request(url,
            {
                method: 'GET',
                headers: opts['headers']
            }, (err, res) => {
                if (!err) {
                    console.log("Response is---->", res.body);
                    // return res.body;
                } else {
                    console.log("err", err)
                }
            });


        return res.send(`<h3>Please check the console for the body.</h3>`);
    },
    sendEmail: (req, res) => {
        //first sign the header
        //the make a post request with the body
        //response will have a message ID


        // const opts = {
        //     service: 'ses', region: 'us-east-1',
        //     path: `/Action=SendEmail&Source=saikrishna%40automate.io&Destination.ToAddresses.member.1=saikrishna%40automate.io&Message.Subject.Data=This%20is%20the%20subject%20line.&Message.Body.Text.Data=Hello.%20I%20hope%20you%20are%20having%20a%20good%20day.`,

        // };
        const opts = { service: 'ses', region: 'us-east-1', path: '/v2/email/outbound-emails' }

        opts.headers = {}

        opts.headers['Content-Type'] = "application/json"
        let body = {
            "ConfigurationSetName": "",
            "Content": {
                "Simple": {
                    "Body": {
                        "Html": {
                            "Charset": "UTF-8",
                            "Data": "<html><body><h1>HelloKrishnathisishtmldata</h1></body></html>"
                        },
                        "Text": {
                            "Charset": "UTF-8",
                            "Data": "Hi this is the ses email"
                        }
                    },
                    "Subject": {
                        "Charset": "UTF-8",
                        "Data": "Testing"
                    }
                }
            },
            "Destination": {
                "ToAddresses": ["krishnasai511@gmail.com"]
            },
            "FromEmailAddress": "saikrishna@automate.io",
        }

        opts["body"] = JSON.stringify(body)

        //by using service and region aws can  infer the host

        aws4.sign(opts, { accessKeyId: '$accces_key', secretAccessKey: 'secret_access_key' });

        console.log("Signed opts-->", opts);

        console.log("\n\n\n")

        /* 
        collect as params
            Region --> we can make url with this ex: 'https://email.{region}.amazonaws.com?Action={path}'
            access_id---
                        ---->  required for signing the header
            secret_key--

            encode the url
        */

        let url = `https://email.${region}.amazonaws.com${opts.path}`


        // opts.headers['Content-Length'] = 230

        // opts.headers['X-Amz-Date'] = '20200625T081329Z'

        //hit this api with request library 


        console.log("URL,opts-->", url, opts)

        request(url,
            opts, (err, res) => {
                if (!err) {
                    console.log("Response is---->", res.body);
                    // return res.body;
                } else {
                    console.log("err", err)
                }
            });
        return res.end()
    },

    getIdentitySDK: (requ, res) => {

        const path = '/?Action=ListIdentities&Version=2010-12-01';

        // create a endpoint using Aws instance and pass the url to it.

        const endpoint = `email.${region}.amazonaws.com`

        const aws_endpt = new AWS.Endpoint(endpoint);
        // make a request


        let url = `https://email.${region}.amazonaws.com${path}` //needed for request.uri

        let req = new AWS.HttpRequest(aws_endpt, region); // aws http method 

        // all the params to be in the request object

        req["method"] = "GET"
        req.headers['host'] = endpoint
        req["path"] = path

        // here we sign our request with aws signaturev4

        let signer = new AWS.Signers.V4(req, 'ses');
        signer.addAuthorization(credentials, new Date());

        //after signature the request will be added with authorization header

        console.log("req obj --------->", req);


        // let headers = {
        //     "Authorization": req["headers"]["Authorization"],
        //     "X-Amz-Date": req["headers"]["X-Amz-Date"]
        // }

        console.log("\n\n\n")
        // here in  request library url is required field
        request(url,
            req, (err, res) => {
                if (!err) {
                    console.log("Response is new request---->", res.body);
                    // return res.body;
                } else {
                    console.log("err", err)
                }
            });

        // aws client will be used for aws http requests

        // let client = new AWS.HttpClient();
        // client.handleRequest(req, null, (response) => {
        //     if (response) {
        //         console.log("Response ------>", response)
        //     }
        // }, (error) => {
        //     console.log("I am error--->", error)
        // });


        return res.end()

    },
    sendEmailSDK: (requ, res) => {

        // const path = '/Action=SendEmail';
        const path = `/Action=SendEmail&Source=saikrishna%40automate.io&Destination.ToAddresses.member.1=krishnasai511%40gmail.com&Message.Subject.Data=This%20is%20the%20subject%20line.&Message.Body.Text.Data=Hello%20I%20hope%20you%20are%20having%20a%20good%20day.`

        // create a endpoint using Aws instance and pass the url to it.

        const endpoint = `email.${region}.amazonaws.com`
        const aws_endpt = new AWS.Endpoint(endpoint);
        // make a request
        let url = `https://email.${region}.amazonaws.com${path}` //needed for request.uri

        let req = new AWS.HttpRequest(aws_endpt, region); // aws http method 

        // all the params to be in the request object

        req["method"] = "POST"
        req.headers['Host'] = endpoint
        req.headers['Content-Type'] = "application/x-www-form-urlencoded"
        req["path"] = path.trim()
        // req.headers['Date'] = new Date().toISOString()
        const body = {
            Destination: {
                ToAddresses: ["krishnasai511@gmail.com"]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data:
                            "<html><body><h1>HelloKrishnathisishtmldata</h1></body></html>"
                    },
                    Text: {
                        Charset: "UTF-8",
                        Data: "HelloKrishnathisistextdata"
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Testing"
                }
            },
            Source: "saikrishna@automate.io"
        };
        // req["qs"] = JSON.stringify(body).replace("@", "%40");

        // req["body"] = JSON.stringify(body)
        req.headers['Content-Length'] = JSON.stringify(body).length




        // here we sign our request with aws signaturev4

        let signer = new AWS.Signers.V4(req, 'ses');
        signer.addAuthorization(credentials, new Date());

        //after signature the request will be added with authorization header

        console.log("req obj --------->", req);


        // let headers = {
        //     "Authorization": req["headers"]["Authorization"],
        //     "X-Amz-Date": req["headers"]["X-Amz-Date"]
        // }

        console.log("\n\n\n")
        // here in  request library url is required field
        request(url,
            req, (err, res) => {
                if (!err) {
                    console.log("Response is new request---->", res.body);
                    // return res.body;
                } else {
                    console.log("err", err)
                }
            });

        // aws client will be used for aws http requests

        // let client = new AWS.HttpClient();
        // client.handleRequest(req, null, (response) => {
        //     if (response) {
        //         console.log("Response ------>", response)
        //     }
        // }, (error) => {
        //     console.log("I am error--->", error)
        // });


        return res.end()

    },
    sendEmailSDKv2: (requ, res) => {

        const path = '/v2/email/outbound-emails';


        const endpoint = `email.${region}.amazonaws.com`
        const aws_endpt = new AWS.Endpoint(endpoint);
        // make a request
        let url = `https://email.${region}.amazonaws.com${path}` //needed for request.uri

        let req = new AWS.HttpRequest(aws_endpt, region); // aws http method 

        // all the params to be in the request object

        req["method"] = "POST"
        req.headers['host'] = endpoint
        req.headers['Content-Type'] = "application/json"
        req["path"] = path.trim()

        let body = {
            "Content": {
                "Simple": {
                    "Body": {
                        "Html": {
                            "Charset": "UTF-8",
                            "Data": "<html><body><h1>Hello Krishna this is html data</h1></body></html>"
                        },
                        "Text": {
                            "Charset": "UTF-8",
                            "Data": "Hi this is the ses email"
                        }
                    },
                    "Subject": {
                        "Charset": "UTF-8",
                        "Data": "Testing"
                    }
                }
            },
            "Destination": {
                "ToAddresses": ["saikrishna@automate.io"]
            },
            "FromEmailAddress": "krishnasai511@gmail.com",
        }

        req["body"] = JSON.stringify(body);

        let signer = new AWS.Signers.V4(req, 'ses');
        signer.addAuthorization(credentials, new Date());




        console.log("req obj --------->", req);


        console.log("\n\n\n")
        request(url,
            req, (err, res) => {
                if (!err) {
                    console.log("Response Body---->", res.body);
                    console.log("Response StatusCode---->", res.statusCode);
                    // return res.body;
                } else {
                    console.log("err", err)
                }
            });

        // aws client will be used for aws http requests

        // let client = new AWS.HttpClient();
        // client.handleRequest(req, null, (response) => {
        //     if (response) {
        //         console.log("Response ------>", response)
        //     }
        // }, (error) => {
        //     console.log("I am error--->", error)
        // });


        return res.end()

    },
    sendSDK: (requ, res) => {

        const ses = new AWS.SES({ apiVersion: "2010-12-01" });

        const body = {
            Destination: {
                ToAddresses: ["saikrishna@automate.io"]
            },
            Message: {
                Body: {
                    Html: {
                        Charset: "UTF-8",
                        Data:
                            "<html><body><h1>HelloKrishnathisishtmldata</h1></body></html>"
                    },
                    Text: {
                        Charset: "UTF-8",
                        Data: "HelloKrishnathisistextdata"
                    }
                },
                Subject: {
                    Charset: "UTF-8",
                    Data: "Testing"
                }
            },
            Source: "saikrishna@automate.io"
        };


        const sendEmail = ses.sendEmail(body).promise();

        sendEmail
            .then(resp => {
                console.log("response-------->", resp);
            })
            .catch(error => {
                console.log(error);
            });

        return res.end()

    }


}



// AWS error response
/*
<ErrorResponse>
   <Error>
      <Type>
         Sender
      </Type>
      <Code>
         ValidationError
      </Code>
      <Message>
         Value null at 'message.subject' failed to satisfy constraint: Member must not be null
      </Message>
   </Error>
   <RequestId>
      42d59b56-7407-4c4a-be0f-4c88daeea257
   </RequestId>
</ErrorResponse>
*/